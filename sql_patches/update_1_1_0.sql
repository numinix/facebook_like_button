SET @configuration_group_id=0;
SELECT (@configuration_group_id:=configuration_group_id) 
FROM configuration_group 
WHERE configuration_group_title= 'Facebook Like Button' 
LIMIT 1;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES
(NULL, 'Combined Send Button', 'FACEBOOK_LIKE_BUTTON_SEND', 'false', 'Create a combined Like and Send button?', @configuration_group_id, 110, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),');